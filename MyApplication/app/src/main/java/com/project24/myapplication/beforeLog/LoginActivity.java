package com.project24.myapplication.beforeLog;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.project24.myapplication.R;
import com.project24.myapplication.afterLog.HomeScreenActivity;

public class LoginActivity extends AppCompatActivity {

    String username;
    EditText user_inp;

    String password;
    EditText passw_inp;

    Button login_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        user_inp = findViewById(R.id.input_user_log);
        passw_inp = findViewById(R.id.input_passw_log);

        login_btn = findViewById(R.id.btn_logServer);
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logToServer();
            }
        });
    }

    private void logToServer() {
        username = user_inp.getText().toString();
        password = passw_inp.getText().toString();

        System.out.println(username + " + " + password);
        if (validateUser(username,password)){
            goToHomeScreen();
        } else {
            Context context = getApplicationContext();
            CharSequence text = "Log in failed!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }

    private void goToHomeScreen() {
        Intent intent = new Intent(this, HomeScreenActivity.class);
        startActivity(intent);
    }

    private boolean validateUser(String username, String password) {
        //TODO validate user
        return true;
    }
}
