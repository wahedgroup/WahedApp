package com.project24.myapplication.afterLog;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import com.project24.myapplication.R;

public class HomeScreenActivity extends AppCompatActivity {

    private Toolbar home_toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        home_toolbar = findViewById(R.id.toolbar);
        home_toolbar.inflateMenu(R.menu.homescreen_menu);
    }

}
