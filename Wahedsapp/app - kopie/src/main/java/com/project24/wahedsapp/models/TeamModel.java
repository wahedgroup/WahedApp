package com.project24.wahedsapp.models;

public class TeamModel {
    private String team_name;
    private String description;
    private int user_id;

    public TeamModel(String team_name, String description, int user_id) {
        this.team_name = team_name;
        this.description = description;
        this.user_id = user_id;
    }
}
