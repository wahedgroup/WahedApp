package com.project24.wahedsapp.afterLog.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project24.wahedsapp.ApiConnection;
import com.project24.wahedsapp.R;
import com.project24.wahedsapp.afterLog.HomeScreenActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

public class AllChatsFragment extends Fragment {
    private static final String TAG = "All Chats of User Fragment";
    private RecyclerView recyclerView;

    private HomeScreenActivity curActivity;
    private ApiConnection conn;
    public void setConn(ApiConnection conn) {
        this.conn = conn;
    }
    public void setCurActivity(HomeScreenActivity curActivity) {
        this.curActivity = curActivity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_chats,container,false);
        recyclerView = view.findViewById(R.id.active_chats_list);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            curActivity.getSupportActionBar().setTitle("Home(Active Chats)");
            curActivity.setCurrentChannel(null);
            curActivity.setCurrentTeam(null);
            curActivity.setCurrentFriend(null);
            curActivity.invalidateOptionsMenu();
        }
    }
}
