package com.project24.wahedsapp.models;

public class TeamInviteCodeModel {
    private String team_invite_code;

    public TeamInviteCodeModel(String team_invite_code) {
        this.team_invite_code = team_invite_code;
    }
}
