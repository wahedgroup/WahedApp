package com.project24.wahedsapp.afterLog.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.project24.wahedsapp.R;
import com.project24.wahedsapp.models.FullUserModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PendingList_RVAdapter extends RecyclerView.Adapter<PendingList_RVAdapter.UserTeam_RVViewHolder> {

    private ArrayList<FullUserModel> pendings;
    private Context context;

    private OnItemClickListner_yes yesListener;
    private OnItemClickListener_no noListener;

    public interface OnItemClickListener_no {
        void onItemClick_no(int position);
    }

    public interface OnItemClickListner_yes {
        void onItemClick_yes(int position);
    }

    public void setOnItemClickListener_yes(OnItemClickListner_yes listener){
        yesListener = listener;
    }

    public void setOnItemClickListener_no(OnItemClickListener_no listener){
        noListener = listener;
    }

    public PendingList_RVAdapter(ArrayList<FullUserModel> pendings, Context context) {
        this.pendings = pendings;
        this.context = context;
    }

    public FullUserModel getFriendByIndex(int position){
        return pendings.get(position);
    }

    @NonNull
    @Override
    public UserTeam_RVViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_pending_friend_layout,parent,false);
        return new UserTeam_RVViewHolder(view, yesListener, noListener);
    }

    @Override
    public void onBindViewHolder(@NonNull UserTeam_RVViewHolder holder, int position) {
        holder.friend_name.setText(pendings.get(position).getName());
        holder.friend_email.setText(pendings.get(position).getEmail());
    }

    @Override
    public int getItemCount() {
        if (pendings ==null)return 0;
        return pendings.size();
    }

    public void removeAt(int position) {
        pendings.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, pendings.size());
    }


    public static class UserTeam_RVViewHolder extends RecyclerView.ViewHolder {
        private TextView friend_name, friend_email;
        private Button accept_btn, decline_btn;

        public UserTeam_RVViewHolder(@NonNull View itemView, OnItemClickListner_yes ylistner, OnItemClickListener_no nlistner) {
            super(itemView);
            friend_name = itemView.findViewById(R.id.pending_friend_name);
            friend_email = itemView.findViewById(R.id.pending_friend_email);

            accept_btn = itemView.findViewById(R.id.accept_btn);
            decline_btn = itemView.findViewById(R.id.decline_btn);

            accept_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ylistner!=null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            ylistner.onItemClick_yes(position);
                        }
                    }
                }
            });

            decline_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (nlistner!=null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            nlistner.onItemClick_no(position);
                        }
                    }
                }
            });
        }

    }
}
