package com.project24.wahedsapp.afterLog.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project24.wahedsapp.ApiConnection;
import com.project24.wahedsapp.R;
import com.project24.wahedsapp.afterLog.HomeScreenActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

public class TeamChannelFragment extends Fragment {
    private HomeScreenActivity curActivity;
    private static final String TAG = "Channels of team Fragment";
    private RecyclerView recyclerView;
    private boolean isShown = false;

    private ApiConnection conn;
    private View view;
    public void setConn(ApiConnection conn) {
        this.conn = conn;
    }

    public void setCurActivity(HomeScreenActivity curActivity) {
        this.curActivity = curActivity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_team_channels,container,false);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            curActivity.getSupportActionBar().setTitle("Channel(s) of "+curActivity.getCurrentTeam().getTeam_name());
            curActivity.setCurrentChannel(null);
            recyclerView = view.findViewById(R.id.channel_list);
            if (recyclerView.getAdapter()==null) conn.getChannelTeam(this.recyclerView,curActivity.getCurrentTeam().getId(),curActivity);
            curActivity.invalidateOptionsMenu();
        }
    }
}
