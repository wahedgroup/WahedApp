package com.project24.wahedsapp.models;

import com.google.gson.annotations.SerializedName;

public class FullUserModel {
    private int id;
    private String name;
    @SerializedName("biography")
    private String bio;
    private String email;
    private String invite_code;
    private String avatar_url;
    private String email_verified_at;
    private String last_seen_at;
    private String created_at;
    private String updated_at;
    private Object pivot;

    @Override
    public String toString() {
        if (pivot==null) pivot = "Empty";
        return "FullUserModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", bio='" + bio + '\'' +
                ", email='" + email + '\'' +
                ", invite_code='" + invite_code + '\'' +
                ", avatar_url='" + avatar_url + '\'' +
                ", email_verified_at='" + email_verified_at + '\'' +
                ", last_seen_at='" + last_seen_at + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", pivot=" + pivot.toString() +
                '}';
    }

    public Object getPivot() {
        return pivot;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBio() {
        return bio;
    }

    public String getEmail() {
        return email;
    }

    public String getInvite_code() {
        return invite_code;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public String getEmail_verified_at() {
        return email_verified_at;
    }

    public String getLast_seen_at() {
        return last_seen_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }
}
