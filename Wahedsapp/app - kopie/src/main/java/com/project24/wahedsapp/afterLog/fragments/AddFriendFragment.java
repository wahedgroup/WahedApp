package com.project24.wahedsapp.afterLog.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.project24.wahedsapp.ApiConnection;
import com.project24.wahedsapp.R;
import com.project24.wahedsapp.afterLog.HomeScreenActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class AddFriendFragment extends Fragment {
    private HomeScreenActivity curActivity;
    private ApiConnection conn;
    public void setConn(ApiConnection conn) {
        this.conn = conn;
    }
    public void setCurActivity(HomeScreenActivity curActivity) {
        this.curActivity = curActivity;
    }

    private EditText friend_code;
    private Button add_friend_btn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_friend,container,false);
        friend_code = view.findViewById(R.id.add_friend_code);
        add_friend_btn = view.findViewById(R.id.add_friend_btn);
        add_friend_btn.setOnClickListener(v -> {
            System.out.println(friend_code.getText().toString());
            conn.inviteFriend(friend_code.getText().toString(),curActivity);
        });
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) curActivity.getSupportActionBar().setTitle("Add Friend");
    }
}
