package com.project24.wahedsapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project24.wahedsapp.afterLog.AddTeamActivity;
import com.project24.wahedsapp.afterLog.HomeScreenActivity;
import com.project24.wahedsapp.afterLog.JoinTeamActivity;
import com.project24.wahedsapp.afterLog.adapters.FriendsChat_RVAdapter;
import com.project24.wahedsapp.afterLog.adapters.Message_RVAdapter;
import com.project24.wahedsapp.afterLog.adapters.PendingList_RVAdapter;
import com.project24.wahedsapp.afterLog.adapters.TeamChannel_RVAdapter;
import com.project24.wahedsapp.afterLog.adapters.UserTeam_RVAdapter;
import com.project24.wahedsapp.beforeLog.LoginActivity;
import com.project24.wahedsapp.beforeLog.MainActivity;
import com.project24.wahedsapp.models.FriendIdModel;
import com.project24.wahedsapp.models.FullChannelModel;
import com.project24.wahedsapp.models.FullTeamModel;
import com.project24.wahedsapp.models.FullUserModel;
import com.project24.wahedsapp.models.InviteCodeModel;
import com.project24.wahedsapp.models.JWTtokenModel;
import com.project24.wahedsapp.models.MessageModel;
import com.project24.wahedsapp.models.RegUserModel;
import com.project24.wahedsapp.models.TeamInviteCodeModel;
import com.project24.wahedsapp.models.TeamModel;
import com.project24.wahedsapp.models.UserModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiConnection {

    private WahedsAppAPI wahedsAppAPI;
    private JWTtokenModel jwtTokenModel;
    private String jwt = "";
    private int user_id = 0;
    private OkHttpClient.Builder httpClient;

    private FullUserModel fullUserModel;
    private List<FullTeamModel> joined_teams;
    private List<FullChannelModel> current_team_channels;
    private Message_RVAdapter message_rvAdapter;
    private RecyclerView currentRVmessages;

    public FullUserModel getFullUserModel() {
        return fullUserModel;
    }

    public String getJwt() {
        return jwt;
    }

    public ApiConnection(String jwt){
        this.httpClient = new OkHttpClient.Builder();
        this.jwt = jwt;
        try {
            parseAccessToken(jwt);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        createInstance();
    }

    public ApiConnection(){
        createInstance();
    }

    private void createInstance(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WAConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        wahedsAppAPI = retrofit.create(WahedsAppAPI.class);
    }

    public void joinTeam(Context activity, String team_invite_code){
        TeamInviteCodeModel teamInviteCodeModel= new TeamInviteCodeModel(team_invite_code);
        Call<Object> call = wahedsAppAPI.joinTeam("Bearer "+jwt, teamInviteCodeModel);

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (!response.isSuccessful()){
                    System.out.println("code: " + response.code());
                    return;
                }
                System.out.println(response.body().toString());
                goToHomeScreen(activity);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    public void getMessagesChannel(HomeScreenActivity activity, int channel_id, RecyclerView recyclerView){
        Call<List<MessageModel>> call = wahedsAppAPI.getMessages("Bearer " + jwt, channel_id);

        call.enqueue(new Callback<List<MessageModel>>() {
            @Override
            public void onResponse(Call<List<MessageModel>> call, Response<List<MessageModel>> response) {
                if (!response.isSuccessful()){
                    System.out.println("code: " + response.code());
                    return;
                }
                ArrayList<MessageModel> tmp = (ArrayList<MessageModel>) response.body();
                message_rvAdapter = new Message_RVAdapter(tmp,user_id,activity);
                currentRVmessages = recyclerView;
                recyclerView.setHasFixedSize(true);
                recyclerView.setAdapter(message_rvAdapter);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
                linearLayoutManager.setReverseLayout(true);

                recyclerView.setLayoutManager(linearLayoutManager);
            }

            @Override
            public void onFailure(Call<List<MessageModel>> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }



    public void addMessageToRV(MessageModel messageModel){
//        messageModel.setId(message_rvAdapter.getLastMessage().getId()+1);
        message_rvAdapter.addMessage(messageModel);
    }

    public void sendMessage(HomeScreenActivity activity, int team_id, int channel_id, String message){
        MessageModel messageModel = new MessageModel(user_id,team_id,channel_id,message);
        Call<Object> call = wahedsAppAPI.sendMessage("Bearer"+jwt,messageModel);

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (!response.isSuccessful()){
                    System.out.println("code: " + response.code());
                    return;
                }
                System.out.println(response.body().toString());
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    private void deleteInvite(HomeScreenActivity activity, int friend_id){
        FriendIdModel friendIdModel = new FriendIdModel(friend_id);
        Call<Object> call = wahedsAppAPI.deleteInvite("Bearer " + jwt,friendIdModel);

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (!response.isSuccessful()){
                    System.out.println("code: " + response.code());
                    return;
                }
                System.out.println(response.body().toString());

            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    private void acceptInvite(HomeScreenActivity activity, int friend_id){
        FriendIdModel friendIdModel = new FriendIdModel(friend_id);
        Call<Object> call = wahedsAppAPI.acceptInvite("Bearer " + jwt,friendIdModel);

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (!response.isSuccessful()){
                    System.out.println("code: " + response.code());
                    return;
                }
                System.out.println(response.body().toString());
                if (response.body().toString().equals("1.0")) {
                    activity.setViewPager(4);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    public void getPendingNotifications(HomeScreenActivity activity, RecyclerView pendingList){
        Call<List<FullUserModel>> call = wahedsAppAPI.getPending("Bearer " + jwt);
        call.enqueue(new Callback<List<FullUserModel>>() {
            @Override
            public void onResponse(Call<List<FullUserModel>> call, Response<List<FullUserModel>> response) {
                if (!response.isSuccessful()){
                    System.out.println("code: " + response.code());
                    return;
                }
                System.out.println("Dit is gewoon Pending");
                System.out.println(response.body().toString());

                ArrayList<FullUserModel> pending_list = (ArrayList<FullUserModel>) response.body();
                PendingList_RVAdapter adapter = new PendingList_RVAdapter(pending_list,activity);
                pendingList.setAdapter(adapter);
                adapter.setOnItemClickListener_yes(position -> {
                    acceptInvite(activity,adapter.getFriendByIndex(position).getId());
                    adapter.removeAt(position);
                });
                adapter.setOnItemClickListener_no(position -> {
                    deleteInvite(activity,adapter.getFriendByIndex(position).getId());
                    adapter.removeAt(position);
                });
                pendingList.setLayoutManager(new LinearLayoutManager(activity));
            }

            @Override
            public void onFailure(Call<List<FullUserModel>> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    public void getPendingSendNotifications(HomeScreenActivity activity){
        Call<Object> call = wahedsAppAPI.getPendingSend("Bearer " + jwt);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (!response.isSuccessful()){
                    System.out.println("code: " + response.code());
                    return;
                }
                System.out.println("Dit is PendingSend");
                System.out.println(response.body().toString());
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    public void addTeam(String team_name, String description, Context context){
        TeamModel teamModel = new TeamModel(team_name,description,user_id);
        Call<Object> call = wahedsAppAPI.makeTeam("Bearer "+ jwt,teamModel);

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (!response.isSuccessful()){
                    System.out.println("code: " + response.code());
                    return;
                }
                System.out.println(response.body().toString());
                goToHomeScreen(context);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                System.out.println(t.getMessage());
                makeToast("Creating a Team didn't work", context);
            }
        });
    }

    public void inviteFriend(String invite_code, HomeScreenActivity context){
        InviteCodeModel inviteCodeModel = new InviteCodeModel(invite_code);
        Call<Object> call = wahedsAppAPI.inviteFriend("Bearer "+jwt,inviteCodeModel);

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (!response.isSuccessful()){
                    System.out.println("code: " + response.code());
                    return;
                }
                System.out.println(response.body().toString());
                if (response.body().toString().equals("1.0")){
                    context.setViewPager(4);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                System.out.println(t.getMessage());
                makeToast("The Invite Code is Invalid", context);
            }
        });
    }

    public void getUserTeam(RecyclerView recyclerView, HomeScreenActivity activity, String tag){
        Call<List<FullTeamModel>> call = wahedsAppAPI.userTeams("Bearer "+ jwt);
        System.out.println(tag);
        call.enqueue(new Callback<List<FullTeamModel>>() {
            @Override
            public void onResponse(Call<List<FullTeamModel>> call, Response<List<FullTeamModel>> response) {
                if (!response.isSuccessful()){
                    System.out.println("code: " + response.code());
                    return;
                }
                joined_teams = response.body();
                System.out.println(joined_teams.toString());
                UserTeam_RVAdapter adapter = new UserTeam_RVAdapter((ArrayList<FullTeamModel>) joined_teams,activity);
                recyclerView.setAdapter(adapter);
                adapter.setOnItemClickListener(new UserTeam_RVAdapter.OnItemClickListner() {
                    @Override
                    public void onItemClick(int position) {
                        activity.setCurrentTeam(adapter.getTeam(position));
                        activity.setViewPager(2);
                    }
                });
                recyclerView.setLayoutManager(new LinearLayoutManager(activity));
            }

            @Override
            public void onFailure(Call<List<FullTeamModel>> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    public void getChannelTeam(RecyclerView recyclerView, int team_id, HomeScreenActivity activity){
        Call<List<FullChannelModel>> call = wahedsAppAPI.teamChannels("Bearer "+ jwt,team_id);

        call.enqueue(new Callback<List<FullChannelModel>>() {
            @Override
            public void onResponse(Call<List<FullChannelModel>> call, Response<List<FullChannelModel>> response) {
                current_team_channels = response.body();
                current_team_channels.forEach((e)-> System.out.println(e.toString()));
                TeamChannel_RVAdapter adapter = new TeamChannel_RVAdapter((ArrayList<FullChannelModel>) current_team_channels, activity);
                recyclerView.setAdapter(adapter);
                adapter.setOnItemClickListener(new TeamChannel_RVAdapter.OnItemClickListner() {
                    @Override
                    public void onItemClick(int position) {
                        activity.setCurrentChannel(adapter.getChannelByIndex(position));
                        activity.setViewPager(3);
                    }
                });

                recyclerView.setLayoutManager(new LinearLayoutManager(activity));
            }

            @Override
            public void onFailure(Call<List<FullChannelModel>> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    public void getFriendsUser(RecyclerView recyclerView, HomeScreenActivity activity) {
        Call<List<FullUserModel>> call = wahedsAppAPI.userFriends("Bearer "+ jwt);

        call.enqueue(new Callback<List<FullUserModel>>() {
            @Override
            public void onResponse(Call<List<FullUserModel>> call, Response<List<FullUserModel>> response) {
                if (!response.isSuccessful()){
                    System.out.println("code: " + response.code());
                    return;
                }
                ArrayList<FullUserModel> friendsList = (ArrayList<FullUserModel>) response.body();
                friendsList.forEach(item -> {
                    System.out.println(item.toString());
                });
                FriendsChat_RVAdapter adapter = new FriendsChat_RVAdapter(friendsList,activity);
                recyclerView.setAdapter(adapter);
                adapter.setOnItemClickListener(position -> {
                    activity.setCurrentFriend(adapter.getFriendByIndex(position));
                    activity.setViewPager(3);
                });
                recyclerView.setLayoutManager(new LinearLayoutManager(activity));
            }

            @Override
            public void onFailure(Call<List<FullUserModel>> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    public void getUserInfo(ImageView profilePhoto, TextView username, TextView usermail, HomeScreenActivity activity){
        Call<FullUserModel> call = wahedsAppAPI.userInfo("Bearer "+ jwt);

        call.enqueue(new Callback<FullUserModel>() {
            @Override
            public void onResponse(Call<FullUserModel> call, Response<FullUserModel> response) {
                if (!response.isSuccessful()){
                    System.out.println("code: " + response.code());
                    return;
                }
                fullUserModel = response.body();
                activity.setUserModel(fullUserModel);
                username.setText(fullUserModel.getName()+"("+ fullUserModel.getInvite_code() +")");
                usermail.setText(fullUserModel.getEmail());
                System.out.println(fullUserModel.toString());
            }

            @Override
            public void onFailure(Call<FullUserModel> call, Throwable t) {
                System.out.println(t.getMessage());

            }
        });
    }

    public void regToAPI(String name, String email, String password, Context context){
        RegUserModel regUserModel = new RegUserModel(name,email,password);

        Call<Object> call = wahedsAppAPI.userRegister(regUserModel);

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (!response.isSuccessful()){
                    System.out.println("code: " + response.code());
                    return;
                }
                goToLoginScreen(context);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });
    }

    public void logToAPI(String email, String password, Context context) {
        System.out.println(email + " + " + password);

        UserModel user = new UserModel(email,password);

        Call<JWTtokenModel> call = wahedsAppAPI.userLogin(user);

        call.enqueue(new Callback<JWTtokenModel>() {
            @Override
            public void onResponse(Call<JWTtokenModel> call, Response<JWTtokenModel> response) {
                if (!response.isSuccessful()){
                    System.out.println("code: " + response.code());
                    return;
                }

                jwtTokenModel = response.body();
                System.out.println(jwtTokenModel.getAccess_token());
                jwt = jwtTokenModel.getAccess_token();
                try {
                    parseAccessToken(jwtTokenModel.getAccess_token());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                goToHomeScreen(context);
            }

            @Override
            public void onFailure(Call<JWTtokenModel> call, Throwable t) {
                System.out.println(call.toString());
                System.out.println(t.getMessage());
                makeToast("login failed!", context);
            }
        });
    }


    /**
     * @param access_token String of an access token
     * @throws JSONException    Throws Exception if the output of the access token is invalid
     */
    private void parseAccessToken(String access_token) throws JSONException {
//        this.jwt = access_token;
        String important_info = access_token.split("\\.")[1];
        System.out.println("Belangrijk = "+important_info);
        byte[] decoded_token_array = Base64.decode(important_info,Base64.DEFAULT);
        String output = new String(decoded_token_array);
        JSONObject response_info = new JSONObject(output);
        System.out.println((int) response_info.get("sub"));
        user_id = (int) response_info.get("sub");
    }


    /**
     * @param s message of the Toast
     * @param context   The Activity where the toast will show.
     */
    private void makeToast(String s, Context context) {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, s, duration);
        toast.show();
    }

    private void goToHomeScreen(Context context) {
        Intent intent = new Intent(context, HomeScreenActivity.class);
        intent.putExtra("jwt",this.jwt);
        context.startActivity(intent);
    }

    public void goToAddTeam(Context context) {
        Intent intent = new Intent(context, AddTeamActivity.class);
        intent.putExtra("jwt",this.jwt);
        context.startActivity(intent);
    }

    public void goToJoinTeam(Context context) {
        Intent intent = new Intent(context, JoinTeamActivity.class);
        intent.putExtra("jwt", this.jwt);
        context.startActivity(intent);
    }

    public void goToMain(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
//        intent.putExtra("jwt", this.jwt);
        context.startActivity(intent);
    }


    public void goToLoginScreen(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
