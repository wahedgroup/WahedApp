package com.project24.wahedsapp.afterLog.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.project24.wahedsapp.R;
import com.project24.wahedsapp.models.FullChannelModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TeamChannel_RVAdapter extends RecyclerView.Adapter<TeamChannel_RVAdapter.ChannelTeam_RVViewHolder> {

    private ArrayList<FullChannelModel> channels;
    private Context context;

    private OnItemClickListner mListener;

    public interface OnItemClickListner {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListner listener){
        mListener = listener;
    }

    public TeamChannel_RVAdapter(ArrayList<FullChannelModel> channels, Context context) {
        this.channels = channels;
        this.context = context;
    }

    public FullChannelModel getChannelByIndex(int index){
        return channels.get(index);
    }

    public ArrayList<FullChannelModel> getChannels() {
        return channels;
    }

    @NonNull
    @Override
    public ChannelTeam_RVViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_team_channel_layout,parent,false);
        return new ChannelTeam_RVViewHolder(view,mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ChannelTeam_RVViewHolder holder, int position) {
        holder.channelname.setText(channels.get(position).getChannel_name());
        holder.channeldesc.setText(channels.get(position).getType());
    }

    @Override
    public int getItemCount() {
        if (channels ==null) return 0;
        return channels.size();
    }


    public static class ChannelTeam_RVViewHolder extends RecyclerView.ViewHolder {
        private TextView channelname, channeldesc;

        public ChannelTeam_RVViewHolder(@NonNull View itemView, OnItemClickListner listner) {
            super(itemView);
            channelname = itemView.findViewById(R.id.channel_name);
            channeldesc = itemView.findViewById(R.id.channel_desc);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listner.onItemClick(position);
                        }
                    }
                }
            });
        }

    }
}
