package com.project24.wahedsapp.models;

public class RegUserModel {
    private String name;
    private String avatar_url;
    private String email;
    private String password;

    public RegUserModel(String name, String email, String password) {
        this.name = name;
        this.avatar_url = "";
        this.email = email;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
