package com.project24.wahedsapp.beforeLog;

import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.project24.wahedsapp.R;

public class MainActivity extends AppCompatActivity {

    Button login_btn;
    Button signup_btn;
    Button web_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login_btn = (Button) findViewById(R.id.btn_log);
        signup_btn = (Button) findViewById(R.id.btn_sign);
        web_btn = (Button) findViewById(R.id.btn_web);

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLogin();
            }
        });

        signup_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToRegister();
            }
        });

        web_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToWebPage();
            }
        });
    }

    private void goToWebPage() {
        Intent intent = new Intent( Intent.ACTION_VIEW , Uri.parse("https://wahedsapp.ml/") );
        startActivity(intent);
    }

    private void goToRegister() {
        Intent intent = new Intent(this,RegisterActivity.class);
        startActivity(intent);
    }

    public void goToLogin(){
        Intent intent = new Intent(this,LoginActivity.class);
        startActivity(intent);
    }
}
