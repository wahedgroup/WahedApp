package com.project24.wahedsapp.afterLog;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.project24.wahedsapp.ApiConnection;
import com.project24.wahedsapp.R;

public class AddTeamActivity extends AppCompatActivity {



    private ApiConnection apiConnection;
    private EditText name,description;
    private Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_team);

        name = findViewById(R.id.join_team_code);
        description = findViewById(R.id.add_team_desc);
        submit = findViewById(R.id.join_team_btn);

        this.apiConnection = new ApiConnection(getIntent().getStringExtra("jwt"));
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apiConnection.addTeam(name.getText().toString(),description.getText().toString(),AddTeamActivity.this);
            }
        });
    }
}
