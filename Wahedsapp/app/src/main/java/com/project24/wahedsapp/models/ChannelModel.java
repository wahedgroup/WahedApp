package com.project24.wahedsapp.models;

public class ChannelModel {
    private int team_id;
    private String channel_name;
    private String type;

    public ChannelModel(int team_id, String channel_name, String type) {
        this.team_id = team_id;
        this.channel_name = channel_name;
        this.type = type;
    }
}
