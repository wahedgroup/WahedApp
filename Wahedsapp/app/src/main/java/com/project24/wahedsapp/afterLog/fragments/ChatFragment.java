package com.project24.wahedsapp.afterLog.fragments;

import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.project24.wahedsapp.ApiConnection;
import com.project24.wahedsapp.R;
import com.project24.wahedsapp.afterLog.HomeScreenActivity;
import com.project24.wahedsapp.models.IncomingEvent;
import com.project24.wahedsapp.models.MessageModel;
import com.project24.wahedsapp.models.SimpelUserModel;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.PresenceChannel;
import com.pusher.client.channel.PresenceChannelEventListener;
import com.pusher.client.channel.PrivateChannel;
import com.pusher.client.channel.PrivateChannelEventListener;
import com.pusher.client.channel.PusherEvent;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.channel.User;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

public class ChatFragment extends Fragment {

    private KeyboardView.OnKeyboardActionListener keyListener;
    private RecyclerView messages;
    private Queue<IncomingEvent> msgQueue;
    private EditText msg_input;
    private Button send_btn;

    private HomeScreenActivity curActivity;
    private ApiConnection conn;
    private PrivateChannel channel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat,container,false);

        msgQueue = new LinkedList<>();
        messages = view.findViewById(R.id.message_list);
        msg_input = view.findViewById(R.id.input_msg);
        send_btn = view.findViewById(R.id.btn_send);
        send_btn.setOnClickListener(v -> {
            if (curActivity.getCurrentChannel() != null){
                sendMessage();
            }
        });


        return view;
    }

    private void sendMessage() {
        int team_id = curActivity.getCurrentTeam().getId();
        int channel_id = curActivity.getCurrentChannel().getId();
        String msg = msg_input.getText().toString();
        conn.sendMessage(curActivity, team_id, channel_id, msg);
        msg_input.setText("");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            if (curActivity.getCurrentTeam() != null && curActivity.getCurrentChannel() != null) {
                curActivity.getSupportActionBar().setTitle(curActivity.getCurrentTeam().getTeam_name() + " | " + curActivity.getCurrentChannel().getChannel_name());
                getMessagesChannel();
                System.out.println("channel id = " + curActivity.getCurrentChannel().getId());
            } else if (curActivity.getCurrentFriend() != null) {
                curActivity.getSupportActionBar().setTitle("Chat with " + curActivity.getCurrentFriend().getName());
            }
            curActivity.invalidateOptionsMenu();
        } else {
            if (curActivity.getPusher() != null && curActivity.getCurrentChannel() != null) curActivity.getPusher().unsubscribe("private-team." + curActivity.getCurrentTeam().getId() + ".channel." + curActivity.getCurrentChannel().getId());
        }
    }

    private void getMessagesChannel() {
        conn.getMessagesChannel(curActivity,curActivity.getCurrentChannel().getId(),messages);
        channel = curActivity.getPusher().subscribePrivate("private-team." + curActivity.getCurrentTeam().getId() + ".channel." + curActivity.getCurrentChannel().getId(), new PrivateChannelEventListener() {
            @Override
            public void onAuthenticationFailure(String message, Exception e) {
                System.out.println(message);
                e.printStackTrace();
            }

            @Override
            public void onSubscriptionSucceeded(String channelName) {
                System.out.println("Dit werkt!");
            }

            @Override
            public void onEvent(PusherEvent event) {
                String tmp = event.getData();
                System.out.println(tmp);
                Gson gson = new Gson();
                IncomingEvent incomingEvent = gson.fromJson(tmp, IncomingEvent.class);
                msgQueue.add(incomingEvent);
                MessageModel messageModel = incomingEvent.getMessage();

                System.out.println("Check Deze "+messageModel.toString());
                processMsgQueue();

            }
        },"App\\Events\\NewMessageReceivedEvent");
    }

    private void processMsgQueue() {
        while (!msgQueue.isEmpty()){
            conn.addMessageToRV(msgQueue.poll());
        }
        curActivity.runOnUiThread(() -> goToNewMessage());
    }

    public void goToNewMessage(){
        messages.scrollToPosition(0);
    }

    public void setCurActivity(HomeScreenActivity curActivity) {
        this.curActivity = curActivity;
    }

    public void setConn(ApiConnection conn) {
        this.conn = conn;
    }
}
