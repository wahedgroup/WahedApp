package com.project24.wahedsapp.afterLog.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.project24.wahedsapp.ApiConnection;
import com.project24.wahedsapp.R;
import com.project24.wahedsapp.afterLog.HomeScreenActivity;
import com.project24.wahedsapp.models.FullTeamModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class TeamInfoFragment extends Fragment {
    private HomeScreenActivity curActivity;
    private ApiConnection conn;

    private TextView team_desc, team_name,team_code,team_created_on;
    private ImageView team_avatar;
    private Button team_go_back;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_team_info,container,false);
        this.team_avatar = view.findViewById(R.id.team_info_avatar);
        this.team_name = view.findViewById(R.id.team_info_name);
        this.team_desc = view.findViewById(R.id.team_info_desc);
        this.team_code = view.findViewById(R.id.team_info_code);
        this.team_created_on = view.findViewById(R.id.team_info_created_on);
        this.team_go_back = view.findViewById(R.id.team_info_go_back_btn);
        team_go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                curActivity.setViewPager(2);
            }
        });
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            curActivity.getSupportActionBar().setTitle("Team Info");
            FullTeamModel tmp = curActivity.getCurrentTeam();
            if (tmp != null){
                team_name.setText("Team Name: "+tmp.getTeam_name());
                team_desc.setText(tmp.getDescription());
                team_created_on.setText("Created on: "+tmp.getCreated_at());
                team_code.setText("Invite Code: "+tmp.getInvite_code());
            }
        }
    }

    public void setCurActivity(HomeScreenActivity curActivity) {
        this.curActivity = curActivity;
    }

    public void setConn(ApiConnection conn) {
        this.conn = conn;
    }
}
