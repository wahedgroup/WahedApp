package com.project24.wahedsapp.beforeLog;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.project24.wahedsapp.ApiConnection;
import com.project24.wahedsapp.R;
import com.project24.wahedsapp.WAConstants;
import com.project24.wahedsapp.WahedsAppAPI;
import com.project24.wahedsapp.afterLog.HomeScreenActivity;
import com.project24.wahedsapp.models.JWTtokenModel;
import com.project24.wahedsapp.models.UserModel;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    private String email;
    private EditText email_inp;

    private String password;
    private EditText passw_inp;

    private Button login_btn;

    private ApiConnection api_conn;

    private boolean userlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        api_conn = new ApiConnection();

        email_inp = findViewById(R.id.input_email_log);
        passw_inp = findViewById(R.id.input_passw_log);

        userlogin = false;

        login_btn = findViewById(R.id.btn_logServer);
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = email_inp.getText().toString();
                password = passw_inp.getText().toString();
                api_conn.logToAPI(email,password,LoginActivity.this);
            }
        });
    }


    @Override
    public void onBackPressed() {
        api_conn.goToMain(this);
    }

    /**
     * @param access_token String of an access token
     * @throws JSONException    Throws Exception if the output of the access token is invalid
     */
    private void parseAccessToken(String access_token) throws JSONException {
        String important_info = access_token.split("\\.")[1];
        byte[] decoded_token_array = Base64.decode(important_info,Base64.DEFAULT);
        String output = new String(decoded_token_array);
        JSONObject response_info = new JSONObject(output);
        System.out.println((int) response_info.get("sub"));
//        user_id = (int) response_info.get("sub");
    }


    /**
     * @param s message of the Toast
     * @param context   The Activity where the toast will show.
     */
    private void makeToast(String s, Context context) {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, s, duration);
        toast.show();
    }

    private void goToHomeScreen(Context context) {
        Intent intent = new Intent(context, HomeScreenActivity.class);
        context.startActivity(intent);
    }
}
