package com.project24.wahedsapp.afterLog.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import com.project24.wahedsapp.ApiConnection;
import com.project24.wahedsapp.R;
import com.project24.wahedsapp.afterLog.HomeScreenActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class AddChannelFragment extends Fragment {
    private HomeScreenActivity curActivity;
    private ApiConnection conn;
    public void setConn(ApiConnection conn) {
        this.conn = conn;
    }
    public void setCurActivity(HomeScreenActivity curActivity) {
        this.curActivity = curActivity;
    }

    private EditText channel_name;
    private Switch channel_switch;
    private Button add_channel_btn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_channel,container,false);
        channel_name = view.findViewById(R.id.add_channel_name);
        channel_switch = view.findViewById(R.id.toggle_text_voice);
        add_channel_btn = view.findViewById(R.id.add_channel_btn);
        add_channel_btn.setOnClickListener(v -> {
            System.out.println(channel_name.getText().toString());
            String name = channel_name.getText().toString();
            if (name.contains(" ")) conn.makeToast("You have a space in your name",curActivity);
            else {
                boolean text_voice = channel_switch.isChecked();
                String type = text_voice ? "voice" : "text";
                System.out.println(name + " status = " + type);
                conn.addChannel(name, curActivity.getCurrentTeam().getId(), type, curActivity);
            }
        });
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) curActivity.getSupportActionBar().setTitle("Add Channel");
    }
}
