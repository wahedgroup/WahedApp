package com.project24.wahedsapp.models;

public class JWTtokenModel {
    private String access_token;
    private String token_type;
    private int expires_in;

    public int getExpires_in() {
        return expires_in;
    }

    public String getToken_type() {
        return token_type;
    }

    public String getAccess_token() {
        return access_token;
    }
}
