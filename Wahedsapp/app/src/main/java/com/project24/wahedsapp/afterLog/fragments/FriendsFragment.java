package com.project24.wahedsapp.afterLog.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project24.wahedsapp.ApiConnection;
import com.project24.wahedsapp.R;
import com.project24.wahedsapp.afterLog.HomeScreenActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

public class FriendsFragment extends Fragment {
    private HomeScreenActivity curActivity;
    private static final String TAG = "Friends Fragment";
    private RecyclerView recyclerView;

    private ApiConnection conn;
    public void setConn(ApiConnection conn) {
        this.conn = conn;
    }

    public void setCurActivity(HomeScreenActivity curActivity) {
        this.curActivity = curActivity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends,container,false);
        recyclerView = view.findViewById(R.id.friends_list);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            curActivity.getSupportActionBar().setTitle("All Friends");
            curActivity.setCurrentChannel(null);

            conn.getFriendsUser(recyclerView,curActivity);

            curActivity.invalidateOptionsMenu();    //Update Menu
        }
    }
}
