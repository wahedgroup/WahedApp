package com.project24.wahedsapp.afterLog.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.project24.wahedsapp.R;
import com.project24.wahedsapp.models.FullUserModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class FriendsChat_RVAdapter extends RecyclerView.Adapter<FriendsChat_RVAdapter.UserTeam_RVViewHolder> {

    private ArrayList<FullUserModel> friends;
    private Context context;

    private OnItemClickListner mListener;

    public interface OnItemClickListner {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListner listener){
        mListener = listener;
    }

    public FriendsChat_RVAdapter(ArrayList<FullUserModel> friends, Context context) {
        this.friends = friends;
        this.context = context;
    }

    public FullUserModel getFriendByIndex(int position){
        return friends.get(position);
    }

    @NonNull
    @Override
    public UserTeam_RVViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_friend_chat_layout,parent,false);
        return new UserTeam_RVViewHolder(view,mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull UserTeam_RVViewHolder holder, int position) {
        holder.friendname.setText(friends.get(position).getName());
        holder.last_msg.setText(friends.get(position).getEmail());
    }

    @Override
    public int getItemCount() {
        if (friends ==null)return 0;
        return friends.size();
    }


    public static class UserTeam_RVViewHolder extends RecyclerView.ViewHolder {
        private TextView friendname, last_msg;

        public UserTeam_RVViewHolder(@NonNull View itemView, OnItemClickListner listner) {
            super(itemView);
            friendname = itemView.findViewById(R.id.friend_name);
            last_msg = itemView.findViewById(R.id.last_msg);

            itemView.setOnClickListener(v -> {
                if (listner!=null){
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION){
                        listner.onItemClick(position);
                    }
                }
            });
        }

    }
}
