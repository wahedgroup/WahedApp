package com.project24.wahedsapp.models;

import java.util.ArrayList;

public class FullChannelModel {
    private int id;
    private int team_id;
    private String channel_name;
    private String type;
    private String created_at;
    private String updated_at;
    private FullTeamModel fullTeamModel;

    public FullTeamModel getFullTeamModel() {
        return fullTeamModel;
    }

    public void setFullTeamModel(FullTeamModel fullTeamModel) {
        this.fullTeamModel = fullTeamModel;
    }

    public int getId() {
        return id;
    }

    public int getTeam_id() {
        return team_id;
    }

    public String getChannel_name() {
        return channel_name;
    }

    public String getType() {
        return type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    @Override
    public String toString() {
        return "FullChannelModel{" +
                "id=" + id +
                ", team_id=" + team_id +
                ", channel_name='" + channel_name + '\'' +
                ", type='" + type + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                '}';
    }
}
