package com.project24.wahedsapp.models;

public class SimpelUserModel {
    private int id;
    private String name;
    private String avatar_url;

    @Override
    public String toString() {
        return "SimpelUserModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", avatar_url='" + avatar_url + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAvatar_url() {
        return avatar_url;
    }
}
