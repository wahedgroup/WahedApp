package com.project24.wahedsapp.afterLog.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project24.wahedsapp.ApiConnection;
import com.project24.wahedsapp.R;
import com.project24.wahedsapp.afterLog.HomeScreenActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

public class NotificationsFragment extends Fragment {
    private HomeScreenActivity curActivity;
    private ApiConnection conn;
    private View view;
    private RecyclerView pendingList;

    public void setConn(ApiConnection conn) {
        this.conn = conn;
    }
    public void setCurActivity(HomeScreenActivity curActivity) {
        this.curActivity = curActivity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notifications,container,false);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            curActivity.getSupportActionBar().setTitle("Notifications");
            pendingList = view.findViewById(R.id.pendingList);
            conn.getPendingNotifications(curActivity,pendingList);
            conn.getPendingSendNotifications(curActivity);
        }
    }
}
