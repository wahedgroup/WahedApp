package com.project24.wahedsapp.afterLog.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project24.wahedsapp.ApiConnection;
import com.project24.wahedsapp.R;
import com.project24.wahedsapp.afterLog.HomeScreenActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

public class UserTeamsFragment extends Fragment {
    private static final String TAG = "Teams of user Fragment";
    private RecyclerView recyclerViewTeamList;
    private HomeScreenActivity curActivity;

    private ApiConnection conn;

    public void setConn(ApiConnection conn) {
        this.conn = conn;
    }

    public void setCurActivity(HomeScreenActivity curActivity) {
        this.curActivity = curActivity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_teams,container,false);
        recyclerViewTeamList = view.findViewById(R.id.team_list);
        return view;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            conn.getUserTeam(recyclerViewTeamList, curActivity,TAG);
            curActivity.setCurrentTeam(null);
            curActivity.setCurrentFriend(null);
            curActivity.setCurrentChannel(null);
            curActivity.getSupportActionBar().setTitle("All Teams");
            curActivity.invalidateOptionsMenu();
        }
    }
}
