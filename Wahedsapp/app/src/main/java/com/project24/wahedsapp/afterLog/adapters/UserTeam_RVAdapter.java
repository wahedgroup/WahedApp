package com.project24.wahedsapp.afterLog.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.project24.wahedsapp.R;
import com.project24.wahedsapp.models.FullTeamModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class UserTeam_RVAdapter extends RecyclerView.Adapter<UserTeam_RVAdapter.UserTeam_RVViewHolder> {

    private ArrayList<FullTeamModel> teams;
    private Context context;

    private OnItemClickListner mListener;

    public interface OnItemClickListner {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListner listener){
        mListener = listener;
    }

    public UserTeam_RVAdapter(ArrayList<FullTeamModel> teams, Context context) {
        this.teams = teams;
        this.context = context;
    }

    public FullTeamModel getTeam(int position){
        return teams.get(position);
    }

    @NonNull
    @Override
    public UserTeam_RVViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_user_team_layout,parent,false);
        return new UserTeam_RVViewHolder(view,mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull UserTeam_RVViewHolder holder, int position) {
        holder.teamname.setText(teams.get(position).getTeam_name());
        holder.teamdesc.setText(teams.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        if (teams==null)return 0;
        return teams.size();
    }


    public static class UserTeam_RVViewHolder extends RecyclerView.ViewHolder {
        private TextView teamname, teamdesc;

        public UserTeam_RVViewHolder(@NonNull View itemView, OnItemClickListner listner) {
            super(itemView);
            teamname = itemView.findViewById(R.id.team_name);
            teamdesc = itemView.findViewById(R.id.team_desc);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listner!=null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listner.onItemClick(position);
                        }
                    }
                }
            });
        }

    }
}
