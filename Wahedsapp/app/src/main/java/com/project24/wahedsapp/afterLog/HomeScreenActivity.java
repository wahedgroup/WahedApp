package com.project24.wahedsapp.afterLog;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import com.google.android.material.navigation.NavigationView;
import com.project24.wahedsapp.ApiConnection;
import com.project24.wahedsapp.LockViewPager;
import com.project24.wahedsapp.R;
import com.project24.wahedsapp.WAConstants;
import com.project24.wahedsapp.WahedsAppAPI;
import com.project24.wahedsapp.afterLog.adapters.SectionsStatePagerAdapter;
import com.project24.wahedsapp.afterLog.fragments.AddChannelFragment;
import com.project24.wahedsapp.afterLog.fragments.AddFriendFragment;
import com.project24.wahedsapp.afterLog.fragments.AllChatsFragment;
import com.project24.wahedsapp.afterLog.fragments.ChatFragment;
import com.project24.wahedsapp.afterLog.fragments.FriendsFragment;
import com.project24.wahedsapp.afterLog.fragments.NotificationsFragment;
import com.project24.wahedsapp.afterLog.fragments.TeamChannelFragment;
import com.project24.wahedsapp.afterLog.fragments.TeamInfoFragment;
import com.project24.wahedsapp.afterLog.fragments.UserTeamsFragment;
import com.project24.wahedsapp.models.FullChannelModel;
import com.project24.wahedsapp.models.FullTeamModel;
import com.project24.wahedsapp.models.FullUserModel;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.PrivateChannel;
import com.pusher.client.channel.PrivateChannelEventListener;
import com.pusher.client.channel.PusherEvent;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;
import com.pusher.client.util.HttpAuthorizer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import okhttp3.OkHttpClient;

import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class HomeScreenActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "Homescreen - after login";

    private SectionsStatePagerAdapter sectionsStatePagerAdapter;
    private OkHttpClient.Builder httpClient;

    private ApiConnection api_conn;
    private LockViewPager viewPager;
    private ImageView profilePhoto;
    private TextView username;
    private TextView usermail;

    private FullUserModel userModel;
    private FullTeamModel currentTeam;
    private FullChannelModel currentChannel;
    private FullUserModel currentFriend;
    private Pusher pusher;
    private PrivateChannel notificationsChannel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        profilePhoto = headerView.findViewById(R.id.profilephoto);
        username = headerView.findViewById(R.id.user_name);
        usermail = headerView.findViewById(R.id.user_mail);


        navigationView.setNavigationItemSelectedListener(this);

        //my stuff
        System.out.println(getIntent().getStringExtra("jwt"));
        this.api_conn = new ApiConnection(getIntent().getStringExtra("jwt"));
        this.api_conn.getUserInfo(profilePhoto,username,usermail,this);

        this.viewPager =  (LockViewPager) findViewById(R.id.container);
        setupViewPager(viewPager);

        createPusherInstance();
    }

    private void createPusherInstance() {
        HttpAuthorizer authorizer = new HttpAuthorizer(WAConstants.BASE_URL+"/broadcasting/auth");
        Map<String,String> tmp = new HashMap<>();
        tmp.put("Authorization", "Bearer "+api_conn.getJwt());
        authorizer.setHeaders(tmp);
        PusherOptions options = new PusherOptions().setAuthorizer(authorizer);
        options.setCluster("eu");

        pusher = new Pusher("7fa39c6fd214681153ba", options);

        pusher.connect(new ConnectionEventListener() {
            @Override
            public void onConnectionStateChange(ConnectionStateChange change) {
                Log.i("Pusher", "State changed from " + change.getPreviousState() +
                        " to " + change.getCurrentState());
            }

            @Override
            public void onError(String message, String code, Exception e) {
                Log.i("Pusher", "There was a problem connecting! " +
                        "\ncode: " + code +
                        "\nmessage: " + message +
                        "\nException: " + e
                );
            }
        }, ConnectionState.ALL);

        System.out.println("userid "+api_conn.getUser_id());
        notificationsChannel = pusher.subscribePrivate("private-notifications.user." + api_conn.getUser_id(), new PrivateChannelEventListener() {
            @Override
            public void onAuthenticationFailure(String message, Exception e) {
                System.out.println(message);
                e.printStackTrace();
            }

            @Override
            public void onSubscriptionSucceeded(String channelName) {
                System.out.println("Yeah");
            }

            @Override
            public void onEvent(PusherEvent event) {
                System.out.println(event.toString());
                System.out.println(event.getData());
//                WAConstants.ALERTS.forEach((key,value)->{
//                    if (event.getEventName().contains(key)){
//                        api_conn.makeToast(value,HomeScreenActivity.this);
//                    }
//                });
            }
        },"App\\Events\\NewFriendRequestEvent",
                "App\\Events\\ChannelCreatedEvent",
                "App\\Events\\FriendAcceptedEvent",
                "App\\Events\\FriendCancelledEvent",
                "App\\Events\\FriendDeclinedEvent",
                "App\\Events\\FriendRemovedEvent",
                "App\\Events\\TeamCreatedEvent");
    }

    public Pusher getPusher() {
        return pusher;
    }

    public FullTeamModel getCurrentTeam() {
        return currentTeam;
    }

    public void setCurrentTeam(FullTeamModel currentTeam) {
        this.currentTeam = currentTeam;
    }

    private void setupViewPager(LockViewPager viewPager){
        sectionsStatePagerAdapter = new SectionsStatePagerAdapter(getSupportFragmentManager());

        //INDEX 0
        AllChatsFragment allChatsFragment = new AllChatsFragment();
        allChatsFragment.setConn(api_conn);
        allChatsFragment.setCurActivity(this);
        sectionsStatePagerAdapter.addFragment(allChatsFragment,"Home/AllChats Fragment");

        //INDEX 1
        UserTeamsFragment userTeamsFragment = new UserTeamsFragment();
        userTeamsFragment.setConn(api_conn);
        userTeamsFragment.setCurActivity(this);
        sectionsStatePagerAdapter.addFragment(userTeamsFragment, "TeamsFragment");

        //INDEX 2
        TeamChannelFragment teamChannelFragment = new TeamChannelFragment();
        teamChannelFragment.setConn(api_conn);
        teamChannelFragment.setCurActivity(this);
        sectionsStatePagerAdapter.addFragment(teamChannelFragment, "ChannelsFragment");

        //INDEX 3
        ChatFragment chatFragment = new ChatFragment();
        chatFragment.setConn(api_conn);
        chatFragment.setCurActivity(this);
        sectionsStatePagerAdapter.addFragment(chatFragment,"ChatFragment");

        //INDEX 4
        FriendsFragment friendsFragment = new FriendsFragment();
        friendsFragment.setConn(api_conn);
        friendsFragment.setCurActivity(this);
        sectionsStatePagerAdapter.addFragment(friendsFragment,"Friendsfragment");

        //INDEX 5
        TeamInfoFragment teamInfoFragment = new TeamInfoFragment();
        teamInfoFragment.setConn(api_conn);
        teamInfoFragment.setCurActivity(this);
        sectionsStatePagerAdapter.addFragment(teamInfoFragment,"Team Info Fragment");

        //INDEX 6
        AddFriendFragment addFriendFragment = new AddFriendFragment();
        addFriendFragment.setConn(api_conn);
        addFriendFragment.setCurActivity(this);
        sectionsStatePagerAdapter.addFragment(addFriendFragment,"add Friend Fragment");

        //INDEX 7
        NotificationsFragment notificationsFragment = new NotificationsFragment();
        notificationsFragment.setConn(api_conn);
        notificationsFragment.setCurActivity(this);
        sectionsStatePagerAdapter.addFragment(notificationsFragment,"Notifications Fragment");

        //INDEX 8
        AddChannelFragment addChannelFragment = new AddChannelFragment();
        addChannelFragment.setConn(api_conn);
        addChannelFragment.setCurActivity(this);
        sectionsStatePagerAdapter.addFragment(addChannelFragment,"Add Channel Fragment");

        viewPager.setAdapter(sectionsStatePagerAdapter);
        viewPager.setOffscreenPageLimit(sectionsStatePagerAdapter.getCount());
    }

    public void setViewPager(int fragmentNumber){
        viewPager.setCurrentItem(fragmentNumber);
    }

    public FullUserModel getCurrentFriend() {
        return currentFriend;
    }

    public void setCurrentFriend(FullUserModel currentFriend) {
        this.currentFriend = currentFriend;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (viewPager.getCurrentItem()!=0){
            setViewPager(0);
        }
        else {
            showLogOutPopUp();
        }

    }

    private void showLogOutPopUp() {
        AlertDialog alertbox = new AlertDialog.Builder(this)
                .setMessage("Do you want to exit application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                        pusher.disconnect();
                        api_conn.goToLoginScreen(HomeScreenActivity.this);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                        return;
                    }
                })
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (viewPager.getCurrentItem() == 1) getMenuInflater().inflate(R.menu.menu_teams, menu);
        if (viewPager.getCurrentItem() == 2) getMenuInflater().inflate(R.menu.menu_channels, menu);
        if (viewPager.getCurrentItem() == 4) getMenuInflater().inflate(R.menu.menu_friends, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.add_team_item) {
            api_conn.goToAddTeam(this); // Working!
            return true;
        } else if (id == R.id.join_team_item){
            api_conn.goToJoinTeam(this);
            return true;
        } else if (id == R.id.add_friend_item){
            setViewPager(6);
            return true;
        } else if (id == R.id.delete_friend_item){

            return true;
        } else if (id == R.id.add_channel_item){
            setViewPager(8);
            return true;
        } else if (id == R.id.join_channel_item){

            return true;
        } else if (id == R.id.info_team_item) {
            setViewPager(5);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            setViewPager(0);
        } else if (id == R.id.nav_friends) {
            setViewPager(4);
        } else if (id == R.id.nav_teams) {
            setViewPager(1);
        } else if (id == R.id.nav_notifications) {
            setViewPager(7);
        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_logout) {
            showLogOutPopUp();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public FullUserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(FullUserModel userModel) {
        this.userModel = userModel;
    }

    public FullChannelModel getCurrentChannel() {
        return currentChannel;
    }

    public void setCurrentChannel(FullChannelModel currentChannel) {
        this.currentChannel = currentChannel;
    }
}
