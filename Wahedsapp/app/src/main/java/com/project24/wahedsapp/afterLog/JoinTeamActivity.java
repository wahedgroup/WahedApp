package com.project24.wahedsapp.afterLog;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.project24.wahedsapp.ApiConnection;
import com.project24.wahedsapp.R;

import androidx.appcompat.app.AppCompatActivity;

public class JoinTeamActivity extends AppCompatActivity {

    private ApiConnection apiConnection;
    private EditText invite_code;
    private Button join_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_team);

        this.apiConnection = new ApiConnection(getIntent().getStringExtra("jwt"));

        this.invite_code = findViewById(R.id.join_team_code);
        this.join_btn = findViewById(R.id.join_team_btn);
        this.join_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apiConnection.joinTeam(JoinTeamActivity.this, invite_code.getText().toString());
                System.out.println(invite_code.getText().toString());
            }
        });
    }
}
