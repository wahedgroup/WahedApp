package com.project24.wahedsapp.models;

public class IncomingEvent {
    private SimpelUserModel userData;
    private MessageModel message;

    public SimpelUserModel getUserData() {
        return userData;
    }

    public MessageModel getMessage() {
        return message;
    }
}
