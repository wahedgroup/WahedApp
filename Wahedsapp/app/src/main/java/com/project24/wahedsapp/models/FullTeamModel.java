package com.project24.wahedsapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FullTeamModel {
    private int id;
    private String invite_code;
    private String avatar_url;
    private String team_name;
    private String description;
    @SerializedName("user_id")
    private int owner_id;
    private String created_at;
    private String updated_at;
    private Object pivot;

    public int getId() {
        return id;
    }

    public String getInvite_code() {
        return invite_code;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public String getTeam_name() {
        return team_name;
    }

    public String getDescription() {
        return description;
    }

    public int getOwner_id() {
        return owner_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public Object getPivot() {
        return pivot;
    }

    @Override
    public String toString() {
        return "FullTeamModel{" +
                "id=" + id +
                ", invite_code='" + invite_code + '\'' +
                ", avatar_url='" + avatar_url + '\'' +
                ", team_name='" + team_name + '\'' +
                ", description='" + description + '\'' +
                ", owner_id=" + owner_id +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", pivot=" + pivot +
                '}';
    }
}
