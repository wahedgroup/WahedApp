package com.project24.wahedsapp.beforeLog;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.project24.wahedsapp.ApiConnection;
import com.project24.wahedsapp.R;

public class RegisterActivity extends AppCompatActivity {

    ApiConnection api_conn;
    EditText password, name, email;
    Button submit_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        api_conn = new ApiConnection();

        name = findViewById(R.id.inp_name);
        password = findViewById(R.id.inp_passw);
        email = findViewById(R.id.inp_email);

        submit_user = findViewById(R.id.btn_reg);
        submit_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regToServer();
            }
        });
    }

    private void regToServer() {
        String name_send = name.getText().toString();
        String pass_send = password.getText().toString();
        String email_send = email.getText().toString();
        api_conn.regToAPI(name_send,email_send,pass_send,this);
    }
}
