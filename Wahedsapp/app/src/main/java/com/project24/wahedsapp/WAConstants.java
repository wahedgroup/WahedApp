package com.project24.wahedsapp;

import java.util.HashMap;

public interface WAConstants {
    public static final String BASE_URL = "http://staging.pillowtalk.tk";
    public static final String REG_LOG_TOKEN = "EF8zLsMen8I2shhPdF7EdNzzAFhjOb53NFjliPUC";
    public static final HashMap<String, String> ALERTS = new HashMap<String,String>() {{
                put("App\\Events\\NewFriendRequestEvent","You have received a new friend Request");
                put("App\\Events\\ChannelCreatedEvent","New Channel is created!");
                put("App\\Events\\FriendAcceptedEvent","Friend request is accepted!");
                put("App\\Events\\FriendCancelledEvent","Friend request is cancelled...");
                put("App\\Events\\FriendDeclinedEvent","Friend request is declined...");
                put("App\\Events\\FriendRemovedEvent","A Friend has removed you...");
                put("App\\Events\\TeamCreatedEvent","New Team is created!");
        }};
}
