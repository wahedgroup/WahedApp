package com.project24.wahedsapp;

import com.project24.wahedsapp.models.ChannelModel;
import com.project24.wahedsapp.models.FriendIdModel;
import com.project24.wahedsapp.models.FullChannelModel;
import com.project24.wahedsapp.models.FullTeamModel;
import com.project24.wahedsapp.models.FullUserModel;
import com.project24.wahedsapp.models.IncomingEvent;
import com.project24.wahedsapp.models.InviteCodeModel;
import com.project24.wahedsapp.models.JWTtokenModel;
import com.project24.wahedsapp.models.MessageModel;
import com.project24.wahedsapp.models.RegUserModel;
import com.project24.wahedsapp.models.TeamInviteCodeModel;
import com.project24.wahedsapp.models.TeamModel;
import com.project24.wahedsapp.models.UserModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface WahedsAppAPI {

    @POST("/api/auth/login")
    Call<JWTtokenModel> userLogin(@Body UserModel userModel);

    @POST("/api/auth/register")
    Call<Object> userRegister(@Body RegUserModel regUserModel);

    @GET("/api/auth/me")
    Call<FullUserModel> userInfo(@Header("Authorization") String authToken);

    @GET("/api/userteam")
    Call<List<FullTeamModel>> userTeams(@Header("Authorization") String authToken);

    @POST("/api/teams")
    Call<Object> makeTeam(@Header("Authorization") String authToken,@Body TeamModel teamModel);

    @GET("api/team/{id}/channel")
    Call<List<FullChannelModel>> teamChannels(@Header("Authorization") String authToken, @Path("id") int id);

    @GET("api/friend")
    Call<List<FullUserModel>> userFriends(@Header("Authorization") String authToken);

    @POST("api/friend/invite")
    Call<Object> inviteFriend(@Header("Authorization") String authToken, @Body InviteCodeModel inviteCodeModel);

    @GET("api/friend/pending")
    Call<List<FullUserModel>> getPending(@Header("Authorization") String authToken);

    @GET("api/friend/pending/sent")
    Call<Object> getPendingSend(@Header("Authorization") String authToken);

    @POST("api/friend/accept")
    Call<Object> acceptInvite(@Header("Authorization") String authToken, @Body FriendIdModel friendIdModel);

    @POST("api/friend/delete")
    Call<Object> deleteInvite(@Header("Authorization") String authToken, @Body FriendIdModel friendIdModel);

    @POST("api/message")
    Call<Object> sendMessage(@Header("Authorization") String authToken, @Body MessageModel messageModel);

    @GET("api/channel/{id}/message")
    Call<List<IncomingEvent>> getMessages(@Header("Authorization") String authToken, @Path("id") int id);

    @POST("api/userteam")
    Call<Object> joinTeam(@Header("Authorization") String authToken, @Body TeamInviteCodeModel teamInviteCodeModel);

    @POST("api/team/{id}/channel")
    Call<Object> createChannel(@Header("Authorization") String authToken, @Body ChannelModel channelModel, @Path("id") int id);
}
