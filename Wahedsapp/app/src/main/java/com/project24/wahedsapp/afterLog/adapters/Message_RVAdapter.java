package com.project24.wahedsapp.afterLog.adapters;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.project24.wahedsapp.R;
import com.project24.wahedsapp.models.IncomingEvent;
import com.project24.wahedsapp.models.MessageModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Message_RVAdapter extends RecyclerView.Adapter<Message_RVAdapter.MessageViewHolder> {

    private List<IncomingEvent> messages;
    private Context context;
    private int user_id;

    public Message_RVAdapter(List<IncomingEvent> messages, int user_id, Context context) {
        this.messages = messages;
        this.messages.sort((IncomingEvent a1, IncomingEvent a2) -> {
            if (a1.getMessage().getId() < a2.getMessage().getId()) return 1;
            else return -1;
        });
        this.messages.forEach(item -> {
            System.out.println(item.toString());
        });
        this.context = context;
        this.user_id = user_id;
    }

    public void addMessage(IncomingEvent messageModel){
        messages.add(0,messageModel);
        notifyItemInserted(messages.size() - 1);
        notifyDataSetChanged();
    }

    public IncomingEvent getLastMessage(){
        return messages.get(messages.size()-1);
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_chat_message_layout,parent,false);
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {

        if (messages.get(position).getMessage().getUser_id() == user_id){
            holder.friend_pf.setVisibility(View.INVISIBLE);
            holder.friend_msg.setVisibility(View.INVISIBLE);
            holder.friend_name.setVisibility(View.INVISIBLE);
            holder.user_msg.setVisibility(View.VISIBLE);
            holder.you_name.setVisibility(View.VISIBLE);
            holder.you_name.setText(messages.get(position).getMessage().getTimeStamp() + " You");
            holder.user_msg.setText(messages.get(position).getMessage().getMessage());
        } else {
            holder.user_msg.setVisibility(View.INVISIBLE);
            holder.you_name.setVisibility(View.INVISIBLE);
            holder.friend_msg.setVisibility(View.VISIBLE);
            holder.friend_pf.setVisibility(View.VISIBLE);
            holder.friend_name.setVisibility(View.VISIBLE);
            holder.friend_msg.setText(messages.get(position).getMessage().getMessage());
            if (messages.get(position).getUserData() != null) holder.friend_name.setText(messages.get(position).getUserData().getName() + " "+ messages.get(position).getMessage().getTimeStamp());
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {

        TextView user_msg, friend_msg, friend_name, you_name;
        ImageView friend_pf;

        public MessageViewHolder(@NonNull View itemView) {
            super(itemView);
            user_msg = itemView.findViewById(R.id.message_user);
            friend_msg = itemView.findViewById(R.id.message_friend);
            friend_pf = itemView.findViewById(R.id.friend_photo);
            friend_name = itemView.findViewById(R.id.chat_friend_name);
            you_name = itemView.findViewById(R.id.chat_you_user);
        }
    }
}
