package com.project24.wahedsapp.models;

public class MessageModel {
    private int user_id;
    private int team_id;
    private int channel_id;
    private String message;
    private int id;
    private String created_at;
    private String updated_at;
    private SimpelUserModel user;

    public MessageModel(int user_id, int team_id, int channel_id, String message) {
        this.user_id = user_id;
        this.team_id = team_id;
        this.channel_id = channel_id;
        this.message = message;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SimpelUserModel getUser() {
        return user;
    }

    public int getUser_id() {
        return user_id;
    }

    public int getTeam_id() {
        return team_id;
    }

    public int getChannel_id() {
        return channel_id;
    }

    public String getMessage() {
        return message;
    }

    public int getId() {
        return id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getTimeStamp(){
        String date = created_at.split("T")[0];
        String time = created_at.split("T")[1].substring(0,5);
        return date +" | "+ time;
    }

    @Override
    public String toString() {
        String output = "MessageModel{" +
                "user_id=" + user_id +
                ", team_id=" + team_id +
                ", channel_id=" + channel_id +
                ", message='" + message + '\'' +
                ", id=" + id +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                '}';
        if (created_at == null) created_at = "";
        if (updated_at == null) updated_at = "";
        if (user!=null) output = output.substring(0,output.length()-1)+", user=" + user.toString() +"}";
        return output;
    }
}
